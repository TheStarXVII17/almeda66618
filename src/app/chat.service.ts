import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(public http: HttpClient) { }

  baseUrl = 'https://crudpi.io/747a6c/Yeet';

  postFeed(usernme, msg) {
    const data = {
      username: usernme,
      message: msg
    };
    return this.http.post(this.baseUrl, data);
  }
getFeed() {
  return this.http.get(this.baseUrl);
  }
}
